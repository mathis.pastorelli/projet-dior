import os
import cv2
from dataset import DiorDataset, OutputType, FashionArticle
from preprocessing import *
from grabcut import grabcut_test_image
from extract_features import compare_images, extract_feature
import json
import torch
from torchvision import transforms
from PIL import Image

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

dior_dataset = DiorDataset(output_type=OutputType.FASHION_ARTICLE)
test_images_folder = "data/test_image_headmind_grabcut_B"

categories_json_path = "test_image_headmind_categories.json"
# Load the categories from the JSON file
with open(categories_json_path, "r") as json_file:
    categories_dict = json.load(json_file)

bbox_json_path = "test_set_bounding_boxes.json"
with open(bbox_json_path, "r") as file:
    bb_dict = json.load(file)


def process_all_images(input_folder, output_folder, rect=None):
    for path in [input_folder, output_folder]:
        os.makedirs(path, exist_ok=True)

    i = 0
    for filename in os.listdir(input_folder):
        # Read the image
        image_path = os.path.join(input_folder, filename)
        image = cv2.imread(image_path)

        if filename in bb_dict:
            rect = [val // 10 for val in bb_dict[filename]]
        # Apply GrabCut and save the preprocessed image
        segmented_img = grabcut_test_image(image, rect)
        cv2.imwrite(os.path.join(output_folder, filename), segmented_img)
        print(str(i) + " grabcut done")
        i += 1


def resize_images(input_folder, output_folder, resize_factor):
    for path in [input_folder, output_folder]:
        os.makedirs(path, exist_ok=True)

    for filename in os.listdir(input_folder):
        # Read the image
        image_path = os.path.join(input_folder, filename)
        image = cv2.imread(image_path)

        # Resize the image
        height, width = image.shape[:2]
        new_width = int(width * resize_factor)
        new_height = int(height * resize_factor)
        resized_image = cv2.resize(image, (new_width, new_height))

        # Save the resized image to the test_resized folder
        save_path = os.path.join(output_folder, filename)
        cv2.imwrite(save_path, resized_image)


def compare_all_images(
    filename, dataset=dior_dataset, category_test: str | None = None
):
    scores = {}

    file_path = os.path.join(test_images_folder, filename)

    if category_test is None:
        category_test = categories_dict.get(filename, "Category not found")

    for idx in range(len(dataset)):
        sample = dataset[idx]
        if sample is not None:
            category_dam = sample.category

            scores[sample.article_id] = []

            if category_dam in category_test:
                score = compare_images(sample, file_path)
                print(sample.article_id, score)
                scores[sample.article_id].append(score)

    # Save the results to a JSON file
    json_result_filename = (
        filename.replace(".", "_") + ".json"
    )  # Replace '.' with '_' to ensure a valid filename
    json_result_path = os.path.join("results", json_result_filename)

    # Create the 'results' directory if it doesn't exist
    os.makedirs("results", exist_ok=True)

    with open(json_result_path, "w") as json_file:
        json.dump(scores, json_file)

    print(filename)


def compare_live_image(image, category_test: str, dataset=dior_dataset):
    scores = {}

    for idx in range(len(dior_dataset)):
        sample = dior_dataset[idx]
        if sample is not None:
            category_dam = sample.category

            scores[sample.article_id] = []

            if category_dam in category_test:
                score_b = compare_images(sample, image)

                # print(sample.article_id, score_b)
                scores[sample.article_id].append(score_b)

    return scores


### Décommenter pour faire tourner la première fois
#if __name__ == "__main__":
#    # Loads the dataset
#    resize_images('data/test_image_headmind', 'data/test_image_headmind_resize', 0.1)
#    process_all_images('data/test_image_headmind_resize', 'data/test_image_headmind_grabcut_B')


print("Feature extraction")

if os.path.exists("vectors/"):
    print("Features already extracted")
else:
    i = 0
    for idx in range(len(dior_dataset)):
        img = dior_dataset[idx]
        extract_feature(img)
        print(i)
        i += 1
print("Feature extraction done")


if __name__ == "__main__":
    for filename in os.listdir(test_images_folder):
        compare_all_images(filename)
