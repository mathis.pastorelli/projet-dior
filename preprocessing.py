import cv2
import os


PATH = os.path.join("data", "DAM")

PATH_MIRROR = os.path.join("data", "DAM_mirror")
PATH_ROTATE_90 = os.path.join("data", "DAM_rotate90")
PATH_ROTATE_180 = os.path.join("data", "DAM_rotate180")
PATH_ROTATE_270 = os.path.join("data", "DAM_rotate270")

PATHS_LIST= [PATH_MIRROR, PATH_ROTATE_90, PATH_ROTATE_180, PATH_ROTATE_270]



 
def read_images_names(path = PATH):
    liste_images = []
    for img in os.listdir(path):
        liste_images.append(str(img))

    return liste_images

def read_image(name, path = PATH):
    return cv2.imread(os.path.join(path, name))


def save_image(name, img, path):
    cv2.imwrite(os.path.join(path, name), img)


def mirror(img, image_name):
    save_image(image_name, cv2.flip(img, 1), PATH_MIRROR)
    

def rotate(img, image_name):
    img_90 = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
    save_image(image_name,img_90, PATH_ROTATE_90)

    img_270 = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
    save_image(image_name, img_270, PATH_ROTATE_270 )

    img_180 = cv2.rotate(img, cv2.ROTATE_180)
    save_image(image_name, img_180, PATH_ROTATE_180 )


def resize(image, side_length) :
    #Si side_length est la longueur du côté de l'image
    # (c'est-à-dire l'image est un carré)
    if isinstance(side_length, int):
        shape = (side_length, side_length)
    #Si side_length est un tuple (hauteur, largeur)
    elif isinstance(side_length, tuple):
        shape = side_length
    return cv2.resize(image, shape)


def process_all_images():
    for path in PATHS_LIST:
        os.makedirs(path, exist_ok=True)

    for image_name in read_images_names():
        img = read_image(image_name)
        
        mirror(img, image_name)
        rotate(img, image_name)


if __name__== "__main__" :
    process_all_images()



