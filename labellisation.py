import os
import pandas as pd
import shutil

def creer_dataC_et_parcourir_data():

    csv_path=os.path.join("data", "product_list.csv")
    data_c_path=os.path.join("data", "data_c")
    data_DAM_path=os.path.join("data", "DAM")

    ## transformer le CSV
    product=pd.read_csv(csv_path,sep=',', names=["product_id","gender_class"])
    product[["gender","class"]]=product["gender_class"].str.split(' ',expand=True)
    product.loc[product['class'].isnull(), 'class'] = product.loc[product['class'].isnull(), 'gender']

    # Créer un nouveau dossier s'il n'existe pas déjà
    if not os.path.exists(data_c_path):
        os.makedirs(data_c_path)
    
    if os.path.exists(data_DAM_path):
        for filename in os.listdir(data_DAM_path):
            # Chemin complet de l'élément dans le dossier source
            chemin_fichier_source = os.path.join(data_DAM_path, filename)

            filename=filename.split('.')[0] # enlever le .jpeg dans le nom du fichier
            class_name_prod=product[product["product_id"]==filename]["class"].iloc[0]

            dossier_destination=os.path.join(data_c_path, str(class_name_prod))
            if not os.path.exists(dossier_destination):
                    os.makedirs(dossier_destination)

            shutil.copy(chemin_fichier_source, dossier_destination)
    return()


# dossier_source_csv = r"chemin/vers/votre/dossier/source"
# dossier_destination = r"C:\Users\lisa\projet-dior"

creer_dataC_et_parcourir_data()
