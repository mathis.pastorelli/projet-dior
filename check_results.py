import pandas
import json
import os
import numpy as np
import operator

df = pandas.read_csv("test_labels.csv", sep=", ")

naze = 0
trouves = 0
for i in range(76):
    name = df["NOM"][i]
    try:
        f = open(os.path.join("results", name+"_jpg"+'.json'))
        scores = json.load(f)
        scores = [(k,v) for k,v in scores.items() if v != []]
        scores = sorted(scores, key=lambda x: x[1], reverse=True)

        for k in range(len(scores)):
            if scores[k][0] == df["LABEL"][i]:
                print("Classement du score du bon produit : ", k)
                trouves += 1
                break
        else:
            print("Le produit n'est pas dans la liste des scores")
            naze +=1
    except:
        print("Le json n'existe pas")



print("Produits trouvés : ", trouves)