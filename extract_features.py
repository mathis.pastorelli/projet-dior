import os
import torch
from torchvision import transforms
from torchvision import transforms
import pickle
from PIL import Image

from dataset import FashionArticle
from predict_article_class import model

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model.eval()


def extract_feature(image1: FashionArticle):
    os.makedirs("vectors", exist_ok=True)
    transform = transforms.Compose([transforms.Resize(256), transforms.CenterCrop(224)])
    input = transform(image1.image).unsqueeze(0).to(torch.float32).to(device)
    features = model(input)

    with open(os.path.join("vectors", image1.article_id + ".pickle"), "wb") as fichier:
        pickle.dump(features, fichier)


def compare_images(image1: FashionArticle, image_test: str):
    # Read the test image
    if type(image_test) == str:
        image2 = Image.open(image_test)
    else:
        image2 = image_test

    transform2 = transforms.Compose(
        [transforms.Resize(256), transforms.CenterCrop(224), transforms.PILToTensor()]
    )

    input2 = transform2(image2).unsqueeze(0).to(torch.float32).to(device)

    # Extract features
    with torch.no_grad():
        with open(
            os.path.join("vectors", image1.article_id + ".pickle"), "rb"
        ) as fichier:
            features1 = pickle.load(fichier)
        features2 = model(input2)

    # Compare images
    similarity_score = torch.nn.functional.cosine_similarity(features1, features2)

    return similarity_score.item()