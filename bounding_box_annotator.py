import os
import matplotlib.pyplot as plt
import cv2
from preprocessing import read_images_names
import json 

FOLDER_TO_ANNOTATE_PATH = os.path.join("data", "test_image_headmind")
    


def select_box(image_path):
    """
    Lets you define a boudning box for an image_path by selecting upper left corner and lower right corner of the image
    """
    # Read the image
    image = cv2.imread(image_path)
    image_copy = image.copy()

    # Create a window to display the image
    cv2.namedWindow('Select Box', cv2.WINDOW_GUI_NORMAL)
    cv2.imshow('Select Box', image_copy)

    # List to store selected points
    points = []

    # Mouse callback function
    def select_point(event, x, y, flags, param):
        nonlocal points, image_copy

        if event == cv2.EVENT_LBUTTONDOWN:
            points.append((x, y))
            if len(points) == 2:
                cv2.rectangle(image_copy, points[0], points[1], (0, 255, 0), 2)
                cv2.imshow('Select Box', image_copy)

    cv2.setMouseCallback('Select Box', select_point)

    # Wait for two points to be selected
    while len(points) < 2:
        cv2.waitKey(1)

    # Close the window
    cv2.destroyAllWindows()

    return points





def annotate(folder_path=FOLDER_TO_ANNOTATE_PATH, result_file_name="bounding_boxes.json"):
    """
    Iterates through image files in folder_path and lets you select a bounding box with 2 points for each image.
    The bounding box selected is shown right after each image for verification purposes.
    Result is saved in a json file with key = image file name and value = bounding box coordinates:[[x,y],[width, height]]

    """

    
    images_names_list = read_images_names(path = folder_path)
    bb_dict = {}
    for img_name in images_names_list:
        # Path to your image
        image_path = os.path.join(folder_path, img_name) 

        # Select points for the box
        selected_box = select_box(image_path)

        # Change format of bounding box from [point1, point2] to [x, y, width, length]
        list_tmp = list(selected_box[0])
        list_tmp.append(-selected_box[0][0]+selected_box[1][0])
        list_tmp.append(-selected_box[0][1]+selected_box[1][1])
        bb_dict[img_name]=list_tmp
        print(list_tmp)

        # Display selected box
        print("Selected Box Points:", selected_box)

        # Plot the image and selected box
        image = cv2.imread(image_path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        plt.imshow(image)
        if len(selected_box) == 2:
            plt.gca().add_patch(plt.Rectangle(selected_box[0], selected_box[1][0] - selected_box[0][0], 
                                            selected_box[1][1] - selected_box[0][1], 
                                            linewidth=2, edgecolor='r', facecolor='none'))
        plt.title('Selected Box')
        plt.show()

    with open(result_file_name, "w") as outfile: 
        json.dump(bb_dict, outfile)

annotate(folder_path=os.path.join("data", "test_image_headmind_small"), result_file_name="bounding_boxes_small.json")