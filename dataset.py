import io
import os
import requests
import zipfile
from dataclasses import dataclass
from enum import Enum

import pandas as pd
import torch
import torchvision.transforms as transforms
from torch.utils.data import Dataset
from torchvision.io import read_image


@dataclass
class FashionArticle:
    image: torch.Tensor
    category: str
    article_id: str


class OutputType(Enum):
    TUPLE = "tuple"
    FASHION_ARTICLE = "fashion_article"


class DiorDataset(Dataset):
    def __init__(
        self,
        csv_file: str = os.path.join("data", "product_list.csv"),
        img_dir: str = os.path.join("data", "DAM"),
        transform=None,
        download: bool = False,
        download_dir: str = "data",
        output_type: OutputType = OutputType.FASHION_ARTICLE,
    ):
        """Pytorch dior dataset.

        Args:
            csv_file: the path to the annotations file
            img_dir: the path to the data directory
            transform: optional transforms
            download: whether to download the dataset or not
            download_dir: the path to the directory where data should be downloaded
            output_type: the type of the output, either a tuple of (tensor, label) or a FashionArticle
        """
        if download:
            r = requests.get("https://pastorelli.xyz/ml/dior/data.zip")
            z = zipfile.ZipFile(io.BytesIO(r.content))
            z.extractall(".")
        self.img_labels = pd.read_csv(csv_file)
        self.img_dir = img_dir
        fpath = lambda name: os.path.join(self.img_dir, f"{name}.jpeg")
        file_exists = lambda name: os.path.exists(fpath(name))
        self.img_labels = self.img_labels[self.img_labels["MMC"].apply(file_exists)]
        self.transform = transform
        self.output_type = output_type
        self.classes = self.img_labels.Product_BusinessUnitDesc.unique().tolist()

    def __len__(self) -> int:
        return len(self.img_labels)

    def class_num(self, label: str) -> int:
        return self.classes.index(label)

    def class_nums(self) -> dict[int, str]:
        return {i: label for i, label in enumerate(self.classes)}

    def class_repartition(self, normalize: bool = False) -> dict[str, int | float]:
        return self.img_labels.Product_BusinessUnitDesc.value_counts(
            normalize=normalize
        ).to_dict()

    def __getitem__(self, idx) -> FashionArticle | tuple:
        article_id = self.img_labels.iloc[idx, 0]
        image_path = os.path.join(self.img_dir, f"{article_id}.jpeg")
        image = read_image(image_path)
        label = self.img_labels.iloc[idx, 1]
        if self.transform:
            to_PIL = transforms.ToPILImage()
            image = to_PIL(image)
            image = self.transform(image)
        if self.output_type == OutputType.TUPLE:
            return (image, torch.tensor(self.class_num(label)))
        return FashionArticle(image=image, category=label, article_id=article_id)
