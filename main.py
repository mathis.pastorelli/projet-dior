import json
import os
from time import time

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec
from matplotlib.patches import Rectangle
from PIL import Image, ImageOps

from cosine_similarity_computation import compare_live_image
from grabcut import grabcut_test_image
from predict_article_class import predict_class, model

base_image_dir = "./data/test_image_headmind"
BOUNDING_BOXES_FILE = "test_set_bounding_boxes.json"


def load_bounding_boxes_dict(path=BOUNDING_BOXES_FILE):
    with open(path) as file:
        return json.load(file)


BOUNDING_BOXES = load_bounding_boxes_dict()


def find_similar_images(
    image, bounding_box: list[list[int, int], list[int, int]], test_image_name: str
):
    # apply grabcut with bounding box

    start = time()

    img_array = np.array(image)

    grabcut_image, bb, _ = grabcut_test_image(img_array, bounding_box)

    x, y, w, h = bb

    grabcut_pil_image = Image.fromarray(grabcut_image)

    first_prediction, _ = predict_class(
        model, grabcut_pil_image, as_str=True
    )

    scores = compare_live_image(grabcut_pil_image, category_test=first_prediction)

    end = time()

    scores = [(k, v[0]) for k, v in scores.items() if len(v) == 1]
    scores = sorted(scores, key=lambda x: x[1], reverse=True)

    fig = plt.figure(figsize=(10, 16))
    outer = gridspec.GridSpec(nrows=1, ncols=2, figure=fig, wspace=0.2, hspace=0.1)

    similar_bags_container = gridspec.GridSpecFromSubplotSpec(
        nrows=5, ncols=2, subplot_spec=outer[1], wspace=0.1, hspace=0.2
    )

    ax = plt.Subplot(fig, outer[0])

    ax.imshow(grabcut_image)
    ax.add_patch(Rectangle((x, y), w, h, fill=False, edgecolor="r"))

    for i, res in enumerate(scores[:10]):
        name, score = res
        aximg = plt.Subplot(fig, similar_bags_container[i])
        img_pred = Image.open(os.path.join("data", "DAM", name + ".jpeg"))
        aximg.imshow(img_pred)
        aximg.text(0, 0, f"{name} \n score: {round(score, 2)}", color="blue")
        aximg.axis("off")
        fig.add_subplot(aximg)

    fig.add_subplot(ax)
    plt.xlabel(
        f"Category: {first_prediction} - prediction time: {round(end-start, 3)}s"
    )
    plt.title(f"Image name : {test_image_name}")
    plt.show()
    plt.clf()
    plt.close()


def find_product(name):
    bbox = BOUNDING_BOXES[name]
    img_file = os.path.join(base_image_dir, name)
    with Image.open(img_file) as image:
        ImageOps.exif_transpose(image, in_place=True)
        return find_similar_images(image, bbox, name)


if __name__ == "__main__":
    for name in sorted(os.listdir(base_image_dir))[20:]:
        find_product(name)
