# imports
import torch
import numpy as np
from sklearn.model_selection import train_test_split
import sys
from torchvision import datasets, transforms, models
import torch.nn as nn
import torch.optim as optim
import os
import matplotlib.pyplot as plt
from PIL import Image
from dataset import DiorDataset

# on lit une première fois les images du dataset
# TODO adapter le path selon l'endroit où sont stockées les données
image_directory = "./data"

print(os.listdir(image_directory))


# __Normalisation__

# Normalisation des images pour les modèles pré-entraînés PyTorch
# voir: https://pytorch.org/docs/stable/torchvision/models.html
# et ici pour les « explications » sur les valeurs exactes: https://github.com/pytorch/vision/issues/1439
mean = np.array([0.485, 0.456, 0.406])
std = np.array([0.229, 0.224, 0.225])

data_transforms = transforms.Compose(
    [
        transforms.Resize([224, 224]),
        transforms.ToTensor(),
        transforms.Normalize(
            mean=mean, std=std
        ),  # On prend la normalisation liée au jeu d'entraînement.
    ]
)

# première lecture des données
dataset_full = DiorDataset(transform=data_transforms)

# some useful info
print("Classes :", dataset_full.classes)
print("Mapping class to index :", dataset_full.class_nums())

# __Partage des données__
# - train = 60 %
# - val = 15 %
# - test = 25 %

# on split en train, val et test à partir de la liste complète
np.random.seed(42)
dataset_train, dataset_test = train_test_split(dataset_full, train_size=0.75)
dataset_train, dataset_val = train_test_split(dataset_train, train_size=0.8)

print("Nombre d'images de train : %i" % len(dataset_train))
print("Nombre d'images de val : %i" % len(dataset_val))
print("Nombre d'images de test : %i" % len(dataset_test))

loader_train = torch.utils.data.DataLoader(
    dataset_train, batch_size=32, shuffle=True, num_workers=4
)

# Vérification : toutes les classes doivent être représentées dans le jeu de données d'entrainement.

# détermination du nombre de classes (nb_classes=6)
# vérification que les labels sont bien dans [0, nb_classes]
labels = [x[1] for x in dataset_train]
if np.min(labels) != 0:
    print("Error: labels should start at 0 (min is %i)" % np.min(labels))
    sys.exit(-1)
if np.max(labels) != (len(np.unique(labels)) - 1):
    print(
        "Error: labels should go from 0 to Nclasses (max label = {}; Nclasse = {})".format(
            np.max(labels), len(np.unique(labels))
        )
    )
    sys.exit(-1)
nb_classes = np.max(labels) + 1

print("Apprentissage sur {} classes".format(nb_classes))


torch.manual_seed(42)


device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


# __Entrainement et évaluation__
#
# On donne une fonction d'entrainement et une fonction d'évaluation (cf TPs précédents).


def evaluate(model, dataset, criterion):
    avg_loss = 0.0
    avg_accuracy = 0
    loader = torch.utils.data.DataLoader(
        dataset, batch_size=16, shuffle=False, num_workers=2
    )
    for data in loader:
        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)
        outputs = model(inputs)

        loss = criterion(outputs, labels)
        _, preds = torch.max(outputs, 1)
        n_correct = torch.sum(preds == labels)

        avg_loss += loss.item()
        avg_accuracy += n_correct

    return avg_loss / len(dataset), float(avg_accuracy) / len(dataset)


PRINT_LOSS = True


def train_model(
    model,
    loader_train,
    data_val,
    optimizer,
    criterion,
    n_epochs=10,
    model_name="resnet18",
):
    for epoch in range(n_epochs):  # à chaque epoch
        print("EPOCH % i" % epoch)
        for i, data in enumerate(loader_train):
            # itère sur les minibatchs via le loader apprentissage
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)
            # on passe les données sur CPU / GPU
            optimizer.zero_grad()  # on réinitialise les gradients
            outputs = model(inputs)  # on calcule l'output

            loss = criterion(outputs, labels)  # on calcule la loss
            if PRINT_LOSS:
                model.train(False)
                loss_val, accuracy = evaluate(my_net, data_val, criterion)
                model.train(True)
                print(
                    "{} loss train: {:1.4f}\t val {:1.4f}\tAcc (val): {:.1%}".format(
                        i, loss.item(), loss_val, accuracy
                    )
                )

            loss.backward()  # on effectue la backprop pour calculer les gradients
            optimizer.step()  # on update les gradients en fonction des paramètres

        torch.save(model.state_dict(), "model_{model_name}_epoch_{epoch}.pth")


# ### MobileNet v2

my_net = models.mobilenet_v2(weights="MobileNet_V2_Weights.IMAGENET1K_V1")
print(my_net)


# remplacement de la couche de classification
my_net.classifier[1] = nn.Linear(
    in_features=my_net.classifier[1].in_features, out_features=nb_classes, bias=True
)
my_net.to(device)

# mise à jour de tous les paramètres
params_to_update = my_net.parameters()

print(sum(p.numel() for p in my_net.parameters()))
print(sum(p.numel() for p in my_net.parameters() if p.requires_grad))

# définition de la fonction de coût et de l'optimiseur
criterion = nn.CrossEntropyLoss()
optimizer = optim.SGD(params_to_update, lr=0.001, momentum=0.9)

# entraînement
print("Apprentissage avec fine-tuning\n")
my_net.train(True)
torch.manual_seed(42)
train_model(my_net, loader_train, dataset_val, optimizer, criterion, n_epochs=10)

# évaluation des performances
my_net.train(False)
loss, accuracy = evaluate(my_net, dataset_test, criterion)
print("\nAccuracy (test): %.1f%%" % (100 * accuracy))
