import torch
import numpy as np
from sklearn.model_selection import train_test_split
import sys
from torchvision import datasets, transforms, models
import torch.nn as nn
import torch.optim as optim
import os
import matplotlib.pyplot as plt
from PIL import Image
from dataset import DiorDataset


import csv
import time

mean = np.array([0.485, 0.456, 0.406])
std = np.array([0.229, 0.224, 0.225])

data_transforms = transforms.Compose(
    [
        transforms.Resize([224, 224]),
        transforms.ToTensor(),
        transforms.Normalize(mean=mean, std=std),
        # On prend la normalisation liée au jeu d'entraînement imagenet
    ]
)


nb_classes = 6

model = models.mobilenet_v2(weights="MobileNet_V2_Weights.IMAGENET1K_V1")
model.classifier[1] = nn.Linear(
    in_features=model.classifier[1].in_features, out_features=nb_classes, bias=True
)
model.load_state_dict(
    torch.load(
        "models/mobilenet_finetuned_on_dior.pth", map_location=torch.device("cpu")
    )
)

model.eval()

dataset_full = DiorDataset()

CLASSES = dataset_full.class_nums()


def predict_class(model, image, preprocess=True, as_str: bool = False):
    """Returns the first two predictions for on given image on a the model."""
    if preprocess:
        image = data_transforms(image)
    img = torch.reshape(image, [1, 3, 224, 224])
    outputs = model(img)
    _, tops = torch.topk(outputs, 2, 1)
    first, second = tops.tolist()[0]
    if as_str:
        first = CLASSES[first]
        second = CLASSES[second]
    return first, second


def test_images_headmind():
    folder = os.path.join("data", "test_image_headmind")
    pred_file = os.path.join("data", "headmind_class_predictions.csv")
    field_names = ["image", "first_prediction", "second_prediction"]
    preds = []

    for img_file in sorted(os.listdir(folder)):
        img_file = os.path.join(folder, img_file)
        with Image.open(img_file) as im:
            f, s = predict_class(model, im, as_str=True)
            preds.append(
                {
                    "image": img_file,
                    "first_prediction": f,
                    "second_prediction": s,
                }
            )
            plt.imshow(im)
            plt.xlabel(f"pred: {f}, 2nd pred: {s}")
            plt.show()
            plt.clf()
            im.close()
            time.sleep(0.2)

    with open(pred_file, "w", newline="") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=field_names)

        writer.writeheader()
        for line in preds:
            writer.writerow(line)


if __name__ == "__main__":
    test_images_headmind()
