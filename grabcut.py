import json
import os
import cv2
import numpy as np
from matplotlib import pyplot as plt

FOLDER_TO_SEGMENT_PATH = os.path.join("data", "test_image_headmind")
RESULT_FOLDER = os.path.join("data", "segmented_test_image_headmind")
BoundingBox_JSON = "test_set_bounding_boxes.json"
RESIZE_FACTOR=10


def show_img_and_bbox(img,rectangle):
    """
    Utils function to display img and its bounding box
    
    """
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    plt.gca().add_patch(plt.Rectangle((rectangle[0], rectangle[1]), rectangle[2] , rectangle[3], linewidth=1, edgecolor='r', facecolor='none'))
    plt.title('Resized Image')
    plt.show()


def grabcut_test_image(image, rect):
    # Create a mask initialized with zeros
    height, width, _ = image.shape
    new_height = 256
    scale_factor = new_height / height
    new_width = int(width * scale_factor)
    rescaled_bbox = [int(scale_factor * i) for i in rect]
    image = cv2.resize(image, (new_width, new_height))
    mask = np.zeros(image.shape[:2], np.uint8)

    # Initialize background and foreground models
    bgdModel = np.zeros((1, 65), np.float64)
    fgdModel = np.zeros((1, 65), np.float64)

    # Apply GrabCut with rectangle initialization
    cv2.grabCut(
        image, mask, rescaled_bbox, bgdModel, fgdModel, 5, cv2.GC_INIT_WITH_RECT
    )

    # Modify mask to create binary mask (0 or 2 corresponds to the background)
    mask2 = np.where((mask == 2) | (mask == 0), 0, 1).astype("uint8")
    anti_mask = np.where((mask == 2) | (mask == 0), 255, 0).astype("uint8")

    # Multiply the original image by the mask to extract the bag
    result = image * mask2[:, :, np.newaxis] + anti_mask[:, :, np.newaxis]

    return result, rescaled_bbox, image
